@echo off
echo type "commit" or "update"
cd "C:\University\4 course\MegaMag\stcap\Sorokina"

set GIT_PATH="C:\Program Files\Git\bin\git.exe"
set BRANCH = "master"

:P
set ACTION=
set /P ACTION=Action: %=%
if "%ACTION%"=="commit" (
  %GIT_PATH% add -A
	%GIT_PATH% commit -am "File changed %date%"
	%GIT_PATH% pull %BRANCH%
	%GIT_PATH% merge %BRANCH%
	%GIT_PATH% push %BRANCH%
)
if "%ACTION%"=="update" (
	%GIT_PATH% pull %BRANCH%
)
if "%ACTION%"=="exit" exit /b
goto P